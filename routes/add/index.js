const user = require('./../../db/Schma');
const Ad = require('./../../db/Schma');
const fs = require('fs');


function fileUnlink(file, callback){

    fs.exists(file, function(exist){
        if (exist){
            fs.unlink(file, function(err){
                if (err){
                    callback('Could not remove file');
                } else {
                    callback(null, 'Success');
                }
            })
        } else {
            callback(null, 'Success');
        }
    });
}


function saveFile(saveDir, fileToUpload, alias, callback){

    var ext = fileToUpload.originalname.substr(fileToUpload.originalname.lastIndexOf('.'));
    var rootDir = '../files/';
    var rootLink = '/files/';

    if (saveDir.indexOf('\\') != -1){
        saveDir.replace(/\\/g, '/');
    }

    if (saveDir[0] == '/'){
        saveDir = saveDir.substr(1, saveDir.length-1);
    }
    if (saveDir[saveDir.length - 1] != '/'){
        saveDir = saveDir + '/';
    }
    var dirArray = saveDir.split('/');

    var fileAlias = fileToUpload.originalname;
    if (alias && (alias != '')) {
        if (alias.indexOf('.') != -1){
            fileAlias = alias;
        } else {
            fileAlias = alias + ext;
        }
    }
    var fileName = rootDir + saveDir + fileAlias;
    var fileLink = rootLink + saveDir + fileAlias;

    var dCounter = 0;
    for (var d=0; d<dirArray.length; d++){

        if (dirArray[d] != ''){

            var subDir = dirArray[d];
            if (d == 0){
                subDir = rootDir + subDir;
            } else {
                var tail = '';
                for (var sd=d; sd>0; sd--){
                    tail = dirArray[sd-1] + '/' + tail;
                }
                subDir = rootDir + tail + subDir;
            }

            checkDir(subDir, function(err){
                if (err){
                    callback('Could not create directory!')
                } else {

                    dCounter ++;
                    if (dCounter == dirArray.length){
                        saveImg(fileName, function(err){
                            if (err){
                                callback(err);
                            } else {
                                callback(null, fileLink);
                            }
                        });
                    }
                }
            });
        } else {

            dCounter ++;
            if (dCounter == dirArray.length){
                saveImg(fileName, function(err){
                    if (err){
                        callback(err);
                    } else {
                        callback(null, fileLink);
                    }
                });
            }
        }
    }

    function checkDir(dir, callback){

        if(!fs.existsSync(dir)){

            fs.mkdir(dir, function(err){
                if (err){
                    callback('Could not make dir');
                } else {
                    callback(null);
                }
            })
        } else {
            callback(null);
        }
    }

    function saveImg(fileName, callback){

        var fileBuffer = fs.readFileSync(fileToUpload.path);

        fs.writeFile(fileName, fileBuffer, function(err){
            if (err){
                console.log('------ Error: ' + err);
                callback('Could not upload file');
            } else {
                fileUnlink(fileToUpload.path, function(err){
                    if (err){
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        })
    }
}

module.exports=function (req, res){ 

    console.log(req.body);

    var set = JSON.parse(req.body.contacts);

  console.log(req.body.json_string);
  console.log(req.files);

    var contact = [];
    for (var i in set) {
        contact.push(set[i]);
    };
    if(req.body.email!==undefined&&req.body.email.length>4&&req.body.email.length<=50){
        if(req.body.title!==undefined&&req.body.title.length>4&&req.body.title.length<=50){
            if(req.body.text!==undefined&&req.body.text.length>4&&req.body.text.length<=1000){
                if(req.body.timeLost!==undefined&&req.body.timeLost.length>4&&req.body.timeLost.length<=250){
                    if(req.body.placeLost!==undefined&&req.body.placeLost.length>4&&req.body.placeLost.length<=250){
                        if(contact!==undefined){
                            if(req.files[0]!=undefined){
                            var newAd = new Ad({ // добавление нового объявления
                                email: req.body.email,
                                title: req.body.title,
                                text: req.body.text,
                                timeLost: req.body.timeLost,
                                placeLost: req.body.placeLost,
                                contacts: contact,
                                type: req.body.type
                            });
                            
                            newAd.save(function(err , list) {
                                if (err) {// вывод сообщения об ошибке
                                    const err_message = require('./../errorOutput')(Object.keys(err.errors)[0], Object.values(err.errors)[0].message);
                                    res.status(200).send(err_message);
                                }
                            else {
                                saveFile('/',req.files[0],String(list._id),function (err,filelink) {
                                  if(err)res.send('Запись "'+ req.body.title + '" была успешно создана! Но с фото произошла ошибка.');else{
                                      list.set({foto:filelink.split('//')[1]});
                                      list.save();
                                      res.send('Запись "'+ list.title + '" была успешно создана!');
                                  }
                                });
                              }
                            }); 
                            }else res.send("Похоже вы забыли про фото.");
                        }else res.send("Похоже что-то не так с вашим полем контакты, проверьте его.");
                    }else res.send("Заполните поле место потери от 5 до 250 символов");
                }else res.send("Заполните поле время потери от 5 до 250 символов");
            }else res.send("Заполните поле полное описание от 5 до 1000 символов");
        }else res.send("Заполните поле заголовок от 5 до 50 символов");
    }else res.send("Заполните поле email от 5 до 50 символов"); 
}
        